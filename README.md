# mysql tick

mkdir -p /var/lib/mysql
Note: If you want to change the default dir above for mysql data storage, then you need to add the new dir in the apparmor config as well in order to use.

Install MySQL server packages and others.

sudo apt-get install mysql-server mysql-client mysql-common
Apply ownership and permissions required.

chown -R mysql.mysql /var/lib/mysql
chmod -R 775 /var/lib/mysql
If you don't want apparmor to make unnecessary issues, better disable it as follows.

sudo service apparmor stop
sudo ln -s /etc/apparmor.d/usr.sbin.mysqld /etc/apparmor.d/disable/
sudo service apparmor restart
sudo aa-status
Now reboot the system.

sudo reboot
Now initialise MySQL as follows.

mysql_install_db
Check everything went fine. You can now use secure installation procedure.

sudo mysql_secure_installation